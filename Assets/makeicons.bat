@REM Make the required .png assets by exporting from icons.svg
@REM Requires https://www.npmjs.com/package/inkscape-export

inkscape-export icons.svg --scale:1 --scale:2
