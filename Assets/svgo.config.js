module.exports = {
  plugins: [
    {
      name: 'preset-default',
      params: {
        overrides: {
          cleanupIDs: false,
          removeHiddenElems: false,
        },
      },
    },
  ],
};