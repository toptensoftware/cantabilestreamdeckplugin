
A StreamDeck plugin to control Cantabile

## DONE

* Transport Control - Play, Pause, Stop
* Record Button - Record, Auto-Record
* Performance Control
    - Live Mode
    - Panic button
    - Continue Button
* Show Notes
    - scroll up/down
* Tempo Button
    - tempo flasher
    - show tempo
    - show time/signature
* Metronome
    - tempo (presets + inc/dec + explicit)
    - time signature (presets + inc/dec + explicit)
    - sounds on/off
* Info
    - Variable Expression
* States
    - next/previous (delay/instant)
    - explicit
    - button grid
    - show states with colors
    - show active item
    - disable pager arrows
    - multiple device
* Set List
    - choose set list button ?
    - next/previous  (delayed/instant)
    - explicit
    - button grid


## TODO
  
* Gain Control
    - choose target (master output, master input, metronome, song, port name)
    - inc/dec (with db delta)
    - predefined steps
    - set explicit (eg: 0db)
        - option to show current value (always, only when changed, only when changed by another button)
    
* Invoke UI Command
* Invoke Arbitrary Binding
* Inject MIDI
* Busy State indicator
* Connection Settings
* Show error state?
* Set list search?
* Countdown Timer
    - long press to reset
    - show remaining time

Later

* Other Misc:
    - page indicators

* Plugin (by index or name)
    - bypass
    - preset next/previous
    - gain
    - hide/show gui
* Rack (by index or name)
    - gain
    - next/previous state
    - invoke custom button

