// Helper to manage the appearance of a Stream Deck button as either an SVG (with 
// modification parameters), or as a wrapped text string and optimized to only pass 
// on to StreamDeck if something has actually changed
function sdImage(context)
{
    let currentKey;

    return {

        // Set the current image from svg
        setSvg: function (url, params)
        {
            if (this.disabled)
            {
                if (!params) params = {};
                params["</svg>"] = '<rect style="fill:#000000;opacity:.75" x="0" y="0" width="20" height="20" /></svg>';
            }

            if (params !== undefined)
            {
                url += url.indexOf('?') >= 0 ? '&' : '?'
                url += util.encodeParams(params);
            }

            if (currentKey != url)
            {
                svgUtils.load(url, svg => $SD.api.setImage(context, svg, 0));
                currentKey = url;
            }
        },

        // Set the current image to text 
        setText: function (value, backColor, foreColor)
        {
            let key = `${value}//${backColor}//${foreColor}//${this.disabled}`;
            if (currentKey != key)
            {
                $SD.api.setImage(context, renderTextCached(value, backColor, foreColor, this.disabled ? "#000000E5" : null), 0);
                currentKey = key;
            }
        }
    }

    // Render a StreamDeck button image with specified text
    function renderText(value, backColor, foreColor, overlay)
    {
        let canvas = document.createElement('canvas');
        canvas.width = 144;
        canvas.height = 144;

        let ctx = canvas.getContext('2d');

        ctx.rect(0, 0, 144, 144);
        ctx.fillStyle = backColor;
        ctx.fill();

        ctx.font = "24pt Arial";
        ctx.fillStyle = foreColor;
        ctx.drawWrappedText(value, 0, 0, 144, 144, "center", "center");

        if (overlay)
        {
            ctx.rect(0, 0, 144, 144);
            ctx.fillStyle = overlay;
            ctx.fill();
        }

        return canvas.toDataURL();
    }

    // Render a StreamDeck button image with specified text (cached)
    function renderTextCached(value, backColor, foreColor, overlay)
    {
        if (!sdImage.cache)
            sdImage.cache = new util.LRUCache(100);

        let key = `${value}//${backColor}//${foreColor}//${overlay}`;

        let e = sdImage.cache.get(key);
        if (!e)
        {
            e = renderText(value, backColor, foreColor, overlay);
            sdImage.cache.set(key, e);
        }

        return e;
    }

}
