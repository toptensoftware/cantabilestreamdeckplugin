// Functions for manipulating SVG images by applying a set of modifications
// in the form of a URL string.  See svgPatch function below for more

var svgUtils = {};

(function ()
{
    // From here: https://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
    function escapeRegExp(string)
    {
        return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
    }
    function escapeReplacement(string)
    {
        return string.replace(/\$/g, '$$$$');
    }
    function replaceAll(text, find, replace)
    {
        return text.replace(new RegExp(escapeRegExp(find), "g"), escapeReplacement(replace));
    }

    // Get attribute from tag string
    function getAttribute(tag, attr)
    {
        let rxAttrib = new RegExp("\\b" + attr + "=\"(.*?)\"");
        let match = tag.match(rxAttrib);
        if (match)
            return match[1];
        else
            return "";
    }

    // Set attribute in tag string
    function setAttribute(tag, attr, value)
    {
        let rxAttrib = new RegExp("\\b" + attr + "=\"(.*?)\"");

        // Try to replace existing attribute
        let found = false;
        let result = tag.replace(rxAttrib, (x) =>
        {
            found = true;
            if (value === null)
                return "";
            else
                return ` ${attr}=\"${value}\"`;
        });

        if (!found)
        {
            result = tag.replace(/(\/)?>$/, (x) => 
            {
                return ` ${attr}=\"${value}\" ${x}`;
            });
        }
        return result;
    }

    // These attributes are stored directly as attributes on the tag.  All others
    // are stored in the style attribute
    let svgPresentationAttributes = {
        "cx": true,
        "cy": true,
        "height": true,
        "width": true,
        "x": true,
        "y": true,
        "r": true,
        "rx": true,
        "ry": true,
        "d": true,
        "transform": true
    };

    // Set an attribute on an SVG tag by setting either the attribute directly, or by modifying the style attribute
    function setSvgAttribute(tag, attr, value)
    {
        // Normal tag attribute?
        if (svgPresentationAttributes[attr])
            return setAttribute(tag, attr, value);

        // Treat as a style attribute

        // Get the old style
        var style = getAttribute(tag, 'style');

        // Modify the style string
        if (!style || style.length == 0)
        {
            if (value === null)
                style = null;
            else
                style = `${attr}:${value}`;
        }
        else
        {
            var rx = new RegExp(`\\b${attr}:[^;]+`, "gm");
            var found = false;
            style = style.replace(rx, (m) =>
            {
                found = true;
                if (value === null)
                    return "";
                else
                    return `${attr}:${value}`
            });
            if (!found && value !== null)
                style = `${style};${attr}:${value}`;
        }

        // Set it
        return setAttribute(tag, 'style', style);
    }


    // Patch an SVG (in string format) by applying a set of modifications in URL query string format.
    //
    // #id.attr=value           - sets the attribute of an element with id
    // .class.attr=value        - sets the attribute of all elements with class
    // x=value                  - performs a global search/replace for string x, setting to value
    //
    // Omit the attribute value to delete the attribute.
    //
    // All attribute names and values are URI decoded before being applied (but after parsing the query string)
    function svgPatch(svgdata, queryString)
    {
        for (let op of queryString.split("&"))
        {
            let eqPos = op.indexOf('=');
            if (eqPos < 0)
                continue;

            var key = decodeURIComponent(op.substr(0, eqPos));
            var val = decodeURIComponent(op.substr(eqPos + 1));
            if (val.length == 0)
                val = null;

            if (key.startsWith('.') || key.startsWith('#'))
            {
                // Split into classOrId and attr name
                let attrPos = key.indexOf('.', 1);
                if (attrPos >= 0)
                {
                    let classOrId = key.substring(1, attrPos);
                    let attrName = key.substr(attrPos + 1);

                    // Create regex to find all matching tags
                    let rxRag;
                    if (key[0] == '.')
                        rxTag = new RegExp(`<([\\w-]+)\\s[^\\0>]*?class=\\".*?\\b${classOrId}\\b.*?\\"[^\\0]*?>`, "gm");
                    else
                        rxTag = new RegExp(`<([\\w-]+)\\s[^\\0>]*?id=\\"${classOrId}\\"[^\\0]*?>`, "gm");

                    svgdata = svgdata.replace(rxTag, (tag) =>
                    {
                        return setSvgAttribute(tag, attrName, val);
                    });

                    continue;
                }
            }

            // Straight replacement
            svgdata = replaceAll(svgdata, key, val);
        }

        return svgdata;
    }

    let svgCache = new util.LRUCache(100);


    // Load an SVG and apply modifications specified in the query string
    // part of the url.  Results are cached.
    svgUtils.load = function (url, callback)
    {
        // Already loaded?
        var e = svgCache.get(url);
        if (e)
        {
            callback(e);
            return;
        }

        // Does it have query string?
        let queryPos = url.indexOf('?');
        if (queryPos >= 0 && queryPos < url.length - 1)
        {
            // Load the unmodified SVG
            svgUtils.load(url.substr(0, queryPos), svg =>
            {

                // Do replacements
                svg = svgPatch(svg, url.substr(queryPos + 1));

                // Cache the modified version
                svgCache.set(url, svg);

                // Send it
                callback(svg);

            });
            return;
        }

        // Setup request
        let req = new XMLHttpRequest();
        req.addEventListener("load", function () 
        {
            // Get the loaded svg text
            let svg = this.responseText;

            // Add data prefix
            svg = "data:image/svg+xml;charset=utf8," + svg;

            // Cache it
            svgCache.set(url, svg);

            // Done
            callback(svg);
        });

        // Send request
        req.open("GET", url);
        req.send();
    }

})();