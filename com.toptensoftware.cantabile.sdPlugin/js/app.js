
// This is a map of handlers for each of the action instances, keyed on the context
let activeActions = new Map();

/* global $SD */
$SD.on('connected', jsn => {
    debugLog('Connected Plugin:', jsn);

    // Request global settings
    $SD.on('didReceiveGlobalSettings', (jsonObj) => {
        connectCantabile(jsonObj?.payload?.settings?.host ?? "http://localhost:35007/");
    });
    $SD.api.getGlobalSettings();

    // Notify all actions when the connection state changes
    C.on('stateChanged', function()
    {
        for (let kv of activeActions)
        {
            kv[1].handler.onConnectionStateChanged?.();
        }
    });

    // This is a map of available actions
    let actions = {
        'com.toptensoftware.cantabile.appactions': appActionsAction,
        'com.toptensoftware.cantabile.countdowntimer': countdownTimerAction,
        'com.toptensoftware.cantabile.gaincontrols': gainControlsAction,
        'com.toptensoftware.cantabile.mediaplayer': mediaPlayerAction,
        'com.toptensoftware.cantabile.metronome': metronomeAction,
        'com.toptensoftware.cantabile.midi': midiAction,
        'com.toptensoftware.cantabile.record': recordAction,
        'com.toptensoftware.cantabile.setlist': setlistAction,
        'com.toptensoftware.cantabile.states': statesAction,
        'com.toptensoftware.cantabile.stringexpression': stringExpressionAction,
        'com.toptensoftware.cantabile.shownotes': showNotesAction,
        'com.toptensoftware.cantabile.tempo': tempoAction,
        'com.toptensoftware.cantabile.transport': transportAction,
        'com.toptensoftware.cantabile.transpose': transposeAction,
    };

    // Register event handlers for all action types
    for (let a in actions)
    {
        $SD.on(a + '.willAppear', jsonObj => {

            //console.log("willAppear", jsonObj.context);

            // Check have payload and settings
            if (!jsonObj.payload || !jsonObj.payload.hasOwnProperty('settings')) 
                return;

            // Create handler
            let handler = actions[a](jsonObj);

            // Cache it
            activeActions.set(jsonObj.context,  {
                json: jsonObj,
                handler: handler
            });

            // Pass it the initial settings
            handler.onDidReceiveSettings?.(jsonObj);
        });

        $SD.on(a + '.didReceiveSettings', jsonObj => {
            const settings = jsonObj.payload.settings;
            const action = activeActions.get(jsonObj.context);
            if(settings && action)
                action.handler.onDidReceiveSettings?.(jsonObj);
        });

        $SD.on(a + '.willDisappear', jsonObj => {
            //console.log("willDisappear", jsonObj.context);

            const action = activeActions.get(jsonObj.context);
            action?.handler.onWillDisappear?.(jsonObj);
        });

        $SD.on(a + '.keyDown', jsonObj => {
            const action = activeActions.get(jsonObj.context);
            action?.handler.onKeyDown?.(jsonObj);
        });

        $SD.on(a + '.keyUp', jsonObj => {
            const action = activeActions.get(jsonObj.context);
            action?.handler.onKeyUp?.(jsonObj);
        });

        $SD.on(a + '.propertyInspectorDidAppear', jsonObj => {
            const action = activeActions.get(jsonObj.context);
            action?.handler.onPropertyInspectorDidAppear?.(jsonObj);
        });

        $SD.on(a + '.sendToPlugin', jsonObj => {
            if (jsonObj.payload?.event == "broadcastSettings")
            {
                const handler = activeActions.get(jsonObj.context);
                jsonObj.device = handler.json.device;
                for (let kv of activeActions)
                {
                    kv[1].handler.onBroadcastSettings?.(jsonObj);
                }
            }
            else
            {
                const action = activeActions.get(jsonObj.context);
                action.handler.onSendToPlugin?.(jsonObj);
            }
        });
    }

    $SD.broadcast = function(device, senderContext, payload)
    {
        for (let kv of activeActions)
        {
            if ((!device || kv[1].json.device == device) && kv[1].json.context != senderContext)
            {
                kv[1].handler.onBroadcast?.(senderContext, payload);
            }
        }
    }

    $SD.api.initDefaultSettings = function(jsonEvent, defaultSettings)
    {
        // Remember the old settings in JSON format
        var oldSettings = jsonEvent?.payload?.settings || {};

        // Apply the old settings over the default settings
        var newSettings = Object.assign({}, defaultSettings, oldSettings);

        // If the settings changed, save them
        if (!_.isEqual(newSettings, oldSettings))
        {
            this.setSettings(jsonEvent.context, newSettings);
        };

        // Return the new settings
        return newSettings;
    };

});