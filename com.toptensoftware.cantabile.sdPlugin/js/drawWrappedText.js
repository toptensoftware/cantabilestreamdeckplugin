// Adds drawWrappedText to canvas 2d.

(function () 
{
    // Canvas extension method to draw wrapped text in rectangle at (x, y, width, height)
    // hAlign can be 'left', 'right', 'center'
    // vAlign can be 'top', 'bottom', 'center'
    CanvasRenderingContext2D.prototype.drawWrappedText = function (text, x, y, width, height, hAlign, vAlign)
    {
        // Get text metrics
        let tm = this.measureText("y");
        if (!tm.fontBoundingBoxAscent)
        {
            tm.fontBoundingBoxAscent = tm.actualBoundingBoxAscent * 1.5;
            tm.fontBoundingBoxDescent = tm.actualBoundingBoxDescent * 1.5;
        }

        let lineHeight = tm.fontBoundingBoxAscent + tm.fontBoundingBoxDescent;

        // Get the lines
        let lines = Array.from(wrapText(this, text, width));

        // Work out horizontal alignment
        let hAlignFunc;
        switch (hAlign)
        {
            case 'right': hAlignFunc = (lineWidth) => Math.max(width - lineWidth, 0); break;
            case 'center': hAlignFunc = (lineWidth) => Math.max((width - lineWidth) / 2, 0); break;
            default: hAlignFunc = (lineWidth) => 0;
        }

        // Work out vertical alignment
        switch (vAlign)
        {
            case 'bottom': y = Math.max(y, y + height - (lines.length * lineHeight)); break;
            case 'center': y = Math.max(y, (y + height - (lines.length * lineHeight)) / 2); break;
            default: y = 0; break;
        }
        y += tm.fontBoundingBoxAscent;

        // Draw lines
        for (var l of lines)
        {
            this.fillText(l.text, x + hAlignFunc(l.width), y);
            y += lineHeight;
        }
    }

    // Helper tp break a line into sections of type 'b' - break (\n"), 's' - spaces, 't' - text (other characters)
    function* findBreaks(text)
    {
        let i = 0;
        while (i < text.length)
        {
            if (text[i] == '\n')
            {
                yield { pos: i, length: 1, kind: 'b' }
                i++;
            }
            else
            {
                let pos = i;
                let whitespace = isSpace(text[i]);
                i++;
                while (i < text.length && isSpace(text[i]) == whitespace && text[i] != '\n')
                    i++;

                yield { pos, length: i - pos, kind: whitespace ? 's' : 't' };
            }
        }

        function isSpace(ch)
        {
            return ch == ' ';
        }
    }

    // Break text at character position so that it fits in maxWidth
    function breakText(ctx, text, maxWidth)
    {
        let l = 0;
        let h = text.length;
        while (h - l > 1)
        {
            let m = l + Math.floor((h - l) / 2);
            let subtext = text.substr(0, m);
            let width = ctx.measureText(subtext).width;
            if (width >= maxWidth)
            {
                h = m;
            }
            else
            {
                l = m;
            }
        }
        return l;
    }

    // Wrap text by return a sequence of lines each with:
    // { 
    //     pos: position in the text where the line starts
    //     length: length of the line in characters
    //     width: width of the line in ctx units
    //     text: the line text
    // }
    function* wrapText(ctx, text, maxWidth)
    {
        if (!text)
            text = "";
        let lineParts = [];

        // Helper function to finish process of the current line
        function finishLine(currentPos)
        {
            // Nothing found before break?
            if (lineParts.length == 0)
            {
                return { pos: currentPos, length: 0, width: 0, text: "" }
            }

            // Remove trailing non-text
            while (lineParts.length > 1 && lineParts[lineParts.length - 1].kind != 't')
                lineParts.splice(lineParts.length - 1, 1);

            // Get the line
            let pos = lineParts[0].pos;
            let length = lineParts[lineParts.length - 1].pos + lineParts[lineParts.length - 1].length - pos;
            let lineText = text.substr(pos, length);
            let width = ctx.measureText(lineText).width;

            // Clear out next line
            lineParts = [];

            // Return result
            return { pos, length, width, text: lineText };
        }

        // Break text into a series of parts
        for (let part of findBreaks(text))
        {
            switch (part.kind)
            {
                case 'b':
                    yield finishLine(part.pos);
                    break;

                case 's':
                case 't':
                    while (true)
                    {
                        // Get the text for the entire line thus far
                        let lineText;
                        if (lineParts.length == 0)
                            lineText = text.substr(part.pos, part.length);
                        else
                            lineText = text.substr(lineParts[0].pos, part.pos + part.length - lineParts[0].pos);

                        // Measure it
                        let lineWidth = ctx.measureText(lineText).width;

                        // If it fits, add this part to the current line and continue with the next part
                        if (lineWidth < maxWidth)
                        {
                            lineParts.push(part);
                            break;
                        }

                        // If it didn't fit, but the line already has parts that do fit, then flush the
                        // current line and try again with the same part
                        if (lineParts.length > 0)
                        {
                            yield finishLine();
                            continue;
                        }

                        // If the part that doesn't fit is whitespace then just ignore it
                        if (part.kind != 't')
                            break;

                        // We're left with a piece of text that's wider than the max width.  Break off the leading part
                        // that does fits, return it as a line and then truncate the start of this part and try again.
                        let breakPos = breakText(ctx, lineText, maxWidth);
                        lineText = lineText.substr(0, breakPos);
                        yield { pos: part.pos, length: breakPos, width: ctx.measureText(lineText).width, text: lineText };

                        // Truncate this part
                        part.pos += breakPos;
                        part.length -= breakPos;
                    }

                    break;
            }
        }

        if (lineParts.length > 0)
            yield finishLine(text.length);
    }

})();
