var util = {};

(function ()
{
    // Implements a simple LRU cache
    let LRUCache = function (capacity)
    {
        this.capacity = capacity;
        this.map = new Map(); // this stores the entire array

        // this is boundaries for double linked list
        this.head = {};
        this.tail = {};

        this.head.next = this.tail; // initialize your double linked list
        this.tail.prev = this.head;
    };

    LRUCache.prototype.get = function (key)
    {
        if (this.map.has(key))
        {
            // remove elem from current position
            let c = this.map.get(key);
            c.prev.next = c.next;
            c.next.prev = c.prev;

            this.tail.prev.next = c; // insert it after last element. Element before tail
            c.prev = this.tail.prev; // update c.prev and next pointer
            c.next = this.tail;
            this.tail.prev = c; // update last element as tail

            return c.value;
        } else
        {
            return null; // element does not exist
        }
    };

    LRUCache.prototype.set = function (key, value)
    {
        if (this.get(key) !== null)
        {
            // if key does not exist, update last element value
            this.tail.prev.value = value;
        }
        else
        {
            // check if map size is at capacity
            if (this.map.size === this.capacity)
            {
                //delete item both from map and DLL
                this.map.delete(this.head.next.key); // delete first element of list
                this.head.next = this.head.next.next; // update first element as next element
                this.head.next.prev = this.head;
            }

            let newNode = {
                value,
                key,
            }; // each node is a hashtable that stores key and value

            // when adding a new node, we need to update both map and DLL
            this.map.set(key, newNode); // add current node to map
            this.tail.prev.next = newNode; // add node to end of the list
            newNode.prev = this.tail.prev; // update prev and next pointers of newNode
            newNode.next = this.tail;
            this.tail.prev = newNode; // update last element
        }
    };

    util.LRUCache = LRUCache;

    // Encode an object as a URL query string
    util.encodeParams = function (obj)
    {
        return Object.keys(obj).map(e => `${encodeURIComponent(e)}=${encodeURIComponent(obj[e])}`).join("&");
    }

    // Split a "#RRGGBBAA" string into components
    function parseRGBA(hex)
    {
        return {
            R: parseInt(getHexComponent(hex, 0), 16),
            G: parseInt(getHexComponent(hex, 1), 16),
            B: parseInt(getHexComponent(hex, 2), 16),
            A: parseInt(getHexComponent(hex, 3), 16),
        }

        function getHexComponent(hex, pos)
        {
            return hex.substr(1 + pos * 2, 2);
        }
    }

    // Format RGBA components into "#RRGGBBAA" string
    function formatRGBA(rgba)
    {
        return `#${formatHexComponent(rgba.R)}${formatHexComponent(rgba.G)}${formatHexComponent(rgba.B)}${formatHexComponent(rgba.A)}`

        function formatHexComponent(val)
        {
            val = Math.round(val);
            if (val < 0) return "00";
            if (val > 255) return "FF";
            return val.toString(16).padStart(2, 0)
        }
    }

    // Blend two colors (each in "#RRGGBBAA" string format)
    util.blendColors = function (back, fore)
    {
        let fore_rgba = parseRGBA(fore);
        let back_rgba = parseRGBA(back);

        if (fore_rgba.A == 255)
            return fore;

        let Aa = fore_rgba.A / 255;
        let Ba = back_rgba.A / 255;
        let OneMinusAa = 1.0 - Aa;

        let blended = {
            R: (fore_rgba.R * Aa / 255 + back_rgba.R * Ba * OneMinusAa / 255) * 255,
            G: (fore_rgba.G * Aa / 255 + back_rgba.G * Ba * OneMinusAa / 255) * 255,
            B: (fore_rgba.B * Aa / 255 + back_rgba.B * Ba * OneMinusAa / 255) * 255,
            A: (Aa + Ba * OneMinusAa) * 255,
        };

        return formatRGBA(blended);
    }

    // Convert decibel value to scalar
    util.db2s = function (db)
    {
        if (db < -60)
            return 0;
        return Math.pow(10.0, db / 20.0);
    }

    // Convert scalar value to decibel
    util.s2db = function (s)
    {
        return 20 * Math.log10(s);
    }

    // Format a scalar gain value in db
    util.formatDb = function (val)
    {
        if (val == 0)
            return "-∞";
        return util.s2db(val).toFixed(1) + " dB";
    }

    // Parse a string of space separated db values
    util.parseDbStops = function (str)
    {
        if (!str)
            return;

        // Parse the split points into a sorted array of scalar values
        let stops = str.split(' ')
            .filter(x => !!x)
            .map(x => x.toLowerCase() == "-inf" ? 0 : util.db2s(Number(x)));
        stops.sort();
        return stops;
    }

    // Find next stop point in a list of values
    util.findNextStop = function (stops, from, dir)
    {
        for (let i = 0; i < stops.length; i++)
        {
            let val = stops[dir < 0 ? stops.length - 1 - i : i];
            let delta = (val - from);

            // Don't get stuck on same value due to float rounding in Cantabile
            if (Math.abs(delta) < 0.00001)
                delta = 0;

            if (delta * dir > 0)
                return val;
        }

        return stops[dir < 0 ? 0 : stops.length - 1];
    }

    // Format a musical interval in Cantabile <octaves>.<semitones> format
    util.formatInterval = function (interval)
    {
        if (!interval)
            return "0";

        let direction = interval < 0 ? -1 : 1;
        interval = Math.abs(interval);

        let octaves = Math.floor(interval / 12);
        let semis = interval % 12;

        return `${direction < 0 ? "-" : "+"}${octaves}.${semis}`;
    }

    // Parse an interval string in Cantabile <octaves>.<semitones> format
    util.parseInterval = function (str)
    {
        if (!str)
            return 0;

        let parts = str.split('.');
        if (parts.length < 1 || parts.length > 2)
            return 0;

        let octaves = parseInt(parts[0]) || 0;
        let semis = parseInt(parts.length == 2 ? parts[1] : 0) || 0;

        let sign = octaves < 0 ? -1 : 1;
        octaves = Math.abs(octaves);

        let interval = (octaves * 12 + semis) * sign;
        if (interval < -127 || interval > 127)
            return 0;

        return interval;
    }

    // Format seconds into "[HH:]MM:SS" format
    util.formatSeconds = function (seconds)
    {
        if (seconds >= 3600)
            return `${Math.floor(seconds / 3600)}:${(Math.floor(seconds / 60) % 60).toString().padStart(2, 0)}:${(seconds % 60).toString().padStart(2, 0)}`;
        else
            return `${Math.floor(seconds / 60).toString().padStart(2, 0)}:${(seconds % 60).toString().padStart(2, 0)}`;
    }

    // Parse a string in [HH:]MM:SS" format into a number of seconds
    util.parseSeconds = function (str)
    {
        if (str.endsWith("h"))
        {
            return Number(str.substring(0, str.length - 1)) * 60 * 60;
        }

        if (str.endsWith("m"))
        {
            return Number(str.substring(0, str.length - 1)) * 60;
        }

        if (str.endsWith("s"))
        {
            return Number(str.substring(0, str.length - 1));
        }

        var parts = str.split(':');
        if (parts.length == 1)
            return parseInt(parts[0]);

        if (parts.length == 2)
            return parseInt(parts[0]) * 60 + parseInt(parts[1]);

        if (parts.length == 3)
            return parseInt(parts[0]) * 3600 + parseInt(parts[1]) * 60 + parseInt(parts[2]);

        return 0;
    }

    // Parse a list of strings in [HH:]MM:SS format
    util.parseSecondsList = function (str)
    {
        if (!str)
            return [];
        // Parse the split points into a sorted array of scalar values
        return str.split(' ')
            .filter(x => !!x)
            .map(x => util.parseSeconds(x));
    }

    // Parse a program number in Cantabile "[[<msb>.]<lsb>.]<prn>" format
    util.parseBankedProgramNumber = function (str)
    {
        let parts = str.split('.');
        let n = 0;
        for (let i = 0; i < parts.length; i++)
        {
            let p = parseInt(parts[i]);
            n = n * 128 + p;
        }
        return n;
    }

    // Parse and validate a time signature string "N/D"
    // Returns null if invalid
    util.parseTimeSig = function (str)
    {
        if (!str)
            return null;
        var parts = str.split('/');
        if (parts.length != 2)
            return null;

        let n = parseInt(parts[0]);
        let d = parseInt(parts[1]);

        if (n < 2 || n > 12)
            return null;

        switch (d)
        {
            case 1:
            case 2:
            case 4:
            case 8:
            case 16:
            case 32:
                break;

            default:
                return null;
        }

        return { n, d };
    }

    // Create an SVG arc descriptor
    // From here: https://stackoverflow.com/a/18473154/77002
    util.describeArc = function (x, y, radius, startAngle, endAngle)
    {
        var start = polarToCartesian(x, y, radius, endAngle);
        var end = polarToCartesian(x, y, radius, startAngle);

        var largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";

        var d = [
            "M", start.x, start.y,
            "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y
        ].join(" ");

        return d;

        function polarToCartesian(centerX, centerY, radius, angleInDegrees)
        {
            var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;
            return {
                x: centerX + (radius * Math.cos(angleInRadians)),
                y: centerY + (radius * Math.sin(angleInRadians))
            };
        }
    }

})();
