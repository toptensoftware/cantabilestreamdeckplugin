// Set default max listeners
Cantabile.EventEmitter.defaultMaxListeners = 64;

// Create Cantabile object
var C = new Cantabile();
C.on('stateChanged', state => console.log(`STATE: ${state}`));

// Open objects
C.application.open();
C.songStates.open();
C.setList.open();
C.song.open();
C.bindings.open();
C.commands.open();

// Connect to Cantabile at specified host url
function connectCantabile(host)
{
    // Disconnect, change host and reconnect
    C.disconnect();
    C.host = host;
    C.connect();
}

let bindingPoints = null;
let sourceBindingPoints = null;
let targetBindingPoints = null;
let audioPortList = null;
let midiPortList = null;
let commandList = null;

C.on('connected', async function ()
{
    // Get list of available binding points
    bindingPoints = await C.bindings.availableBindingPoints();
    bindingPoints.sort((a, b) => a.displayName.localeCompare(b.displayName));
    sourceBindingPoints = bindingPoints.filter(x => x.isSource);
    targetBindingPoints = bindingPoints.filter(x => !x.isSource);

    // Notify
    C.emit('bindingPointsAvailable');

    // Build audio port list
    audioPortList = bindingPoints.filter(x => x.name.startsWith("global.masterLevels.audioPort.") && x.isSource).map(function (x)
    {

        let name = x.name.substring(30);
        let direction = name.startsWith("in.") ? "in" : "out";
        name = name.substring(direction.length + 1);

        return { name, direction, bindingPoint: x.name }

    });

    // Build MIDI port list
    midiPortList = bindingPoints.filter(x => x.name.startsWith("midiOutputPort.") && !x.isSource).map(function (x)
    {

        return { name: x.displayName, bindingPoint: x.name }

    });

    // Notify
    C.emit('portListAvailable');
})

C.on('connected', async function ()
{
    // Get list of available commands
    commandList = await C.commands.availableCommands();
    C.emit('commandListAvailable');
})

C.on('disconnected', () =>
{
    audioPortList = null;
    midiPortList = null;
    commandList = null;
});

// Promise to get all bindings points of particular kind ("source" or "target")
async function GetBindingPoints(kind)
{
    if (bindingPoints != null)
    {
        return Promise.resolve(forKind());
    }

    return new Promise((resolve, reject) =>
    {
        function onBindingsAvailable()
        {
            resolve(forKind());
            C.off('bindingsAvailable', onBindingsAvailable);
        }

        C.on('bindingsAvailable', onBindingsAvailable);
    });

    function forKind()
    {
        if (kind == "source")
            return sourceBindingPoints;
        if (kind == "target")
            return targetBindingPoints;
        return bindingPoints;
    }
}

// Promise to get a list of all audio ports
async function GetAudioPortList()
{
    if (audioPortList != null)
    {
        return Promise.resolve(audioPortList);
    }

    return new Promise((resolve, reject) =>
    {
        function onPortListAvailable()
        {
            resolve(audioPortList);
            C.off('portListAvailable', onPortListAvailable);
        }

        C.on('portListAvailable', onPortListAvailable);
    });
}

// Promise to get a list of all MIDI ports
async function GetMidiPortList()
{
    if (midiPortList != null)
    {
        return Promise.resolve(midiPortList);
    }

    return new Promise((resolve, reject) =>
    {
        function onPortListAvailable()
        {
            resolve(midiPortList);
            C.off('portListAvailable', onPortListAvailable);
        }

        C.on('portListAvailable', onPortListAvailable);
    });
}

// Promise to get a list of all Cantabile UI Commands
async function GetCommandList()
{
    if (commandList != null)
    {
        return Promise.resolve(commandList);
    }

    return new Promise((resolve, reject) =>
    {
        function onCommandListAvailable()
        {
            resolve(commandList);
            C.off('commandListAvailable', onCommandListAvailable);
        }

        C.on('commandListAvailable', onCommandListAvailable);
    });
}

