function midiAction(json)
{
    let settings = json.payload.settings;
    let image = new sdImage(json.context);
    let isActive = false;
    let watching = false;
    let watcher = null;

    function updateImage()
    {
        image.disabled = C.state != "connected";
        let foreColor = settings.foreColor;
        let backColor = settings.backColor;
        if (isActive)
        {
            foreColor = settings.activeForeColor;
            backColor = settings.activeBackColor;
        }
        image.setText(isActive ? settings.activeTitle : settings.title, backColor, foreColor);
    }

    // Create the MIDI payload based on current settings and a specified value
    function makePayload(value)
    {
        if (maxControllerValue() == 0)
            value = 0;

        let controller;
        if (settings.controllerKind == "BankedProgramChange")
            controller = util.parseBankedProgramNumber(settings.controller);
        else
            controller = parseInt(settings.controller)

        return {
            channel: parseInt(settings.channel) - 1,
            kind: settings.controllerKind,
            controller: controller,
            value: value,
        };
    }

    // Work out the max value of a controller based on current settings
    function maxControllerValue()
    {
        switch (settings.controllerKind)
        {
            case "Note":
            case "Controller":
            case "ChannelPressure":
            case "RpnCoarse":
            case "NRpnCoarse":
                return 127;

            case "RpnFine":
            case "NRpnFine":
            case "FineController":
            case "PitchBend":
            case "MasterVolume":
            case "MasterBalance":
                return 16383;
        }
        return 0;
    }

    // Send MIDI event
    function sendMidi(payload)
    {
        if (settings.target == "oskDevice")
            C.onscreenKeyboard.injectMidi(payload);
        else if (settings.target)
            C.bindings.invoke(settings.target, payload);
    }

    // Send MIDI event with specified pressed state
    function sendPressedState(pressed)
    {
        if (pressed)
            sendMidi(makePayload(settings.pressedValue == "" ? maxControllerValue() : parseInt(settings.pressedValue)));
        else
        {
            // Only send the off value when there is an actual controller value
            if (maxControllerValue() != 0)
               sendMidi(makePayload(settings.releasedValue == "" ? 0 : parseInt(settings.releasedValue)));
        }
    }

    // Work out the name of the controller watch point that can be used
    // to monitor cantabile for local changes.
    function watchName()
    {
        switch (settings.controllerKind)
        {
            case "Controller":
            case "FineController":
            case "ProgramChange":
            case "BankedProgramChange":
            case "PitchBend":
            case "ChannelPressure":
            case "RpnCoarse":
            case "RpnFine":
            case "NRpnCoarse":
            case "NRpnFine":
                return settings.controllerKind;
        }
        return null;
    }

    // Receive from Cantabile a change of value of the controller
    function onControllerChanged(value)
    {
        if (settings.controllerKind.endsWith("ProgramChange"))
        {
            isActive = value == settings.controller;
        }
        else
        {
            isActive = value == settings.pressedValue;
        }
        updateImage();
    }

    function startWatching()
    {
        stopWatching();

        // For the oskDevice we can monitor for changes to certain controllers
        // even if we don't make the change ourself.
        if (settings.target == "oskDevice")
        {
            // It's only worth doing for toggle buttons
            if (settings.interaction == "toggleButton" || settings.controllerKind.endsWith("ProgramChange"))
            {
                var watchController = watchName();
                if (watchController)
                {
                    watcher = C.onscreenKeyboard.watch(parseInt(settings.channel) - 1, watchController, parseInt(settings.controller), onControllerChanged);
                }
            }

            // Open OSK device so events sent by websocket instead of http
            C.onscreenKeyboard.open();
            watching = true;
        }
    }

    function stopWatching()
    {
        watcher?.unwatch();
        watcher = null;

        if (watching)
        {
            C.onscreenKeyboard.close();
            watching = false;
        }
    }

    return {

        onKeyUp: function(jsonEvent)
        {
            switch (settings.interaction)
            {
                case "momentaryButton":
                    sendPressedState(false);
                    break;

                case "toggleButton":
                    isActive = !isActive;
                    sendPressedState(isActive);
                    updateImage();
                    break;
            }
        },
        onKeyDown: function(jsonEvent)
        {
            switch (settings.interaction)
            {
                case "button":
                case "momentaryButton":
                    sendPressedState(true);
                    break;
            }
        },
        onDidReceiveSettings: function(jsonEvent)
        {
            settings = $SD.api.initDefaultSettings(jsonEvent, {
                target: "oskDevice",
                interaction: "button",
                controllerKind: "Controller",
                channel: 1,
                controller: 64,
                releasedValue: "",
                pressedValue: "",
                title: "MIDI",
                activeTitle: "MIDI",
                backColor: "#000000",
                foreColor: "#848484",
                activeBackColor: "#0487f3",
                activeForeColor: "#FFFFFF",
            });

            updateImage();
            startWatching();
        },
        onPropertyInspectorDidAppear: async function(jsonEvent)
        {
            // When the property inspector is shown, send it the list
            // of available MIDI ports
            let pl = await GetMidiPortList();
            $SD.api.sendToPropertyInspector(json.context, { portList: pl }, json.action);
        },
        onWillDisappear: function(jsonEvent)
        {   
            stopWatching();
        },
        onConnectionStateChanged: function()
        {
            updateImage();
        }
    }
}