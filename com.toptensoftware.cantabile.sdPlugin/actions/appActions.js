function appActionsAction(json)
{
    let settings = json.payload.settings;
    let image = new sdImage(json.context);
    let watch;
    let watching;
    let isLiveMode = false;
    let attemptingEngineStart = false;
    let longHoldTimeout = false;

    function updateImage()
    {
        let foreColor = settings.foreColor ?? "#848484";
        if (settings.action == "powerButton")
        {
            if (attemptingEngineStart)
            {
                foreColor = "#FF5500";
            }
            else if (C.state == "connected")
            {
                if (C.application.busy)
                    foreColor = "#FF5500";
                else
                    foreColor = "#00FF00";
            }
        }
        else
        {
            image.disabled = C.state != "connected";
        }

        if (isLiveMode)
        {
            foreColor = "#00FF00";
        }

        image.setSvg(`images/appactions_${settings.action}.svg`, {
            "#background.fill": settings.backColor,
            "#848484": foreColor,
        });
    }

    function stopWatching()
    {
        watch?.unwatch();
        watch = null;

        if (watching)
        {
            C.application.off('busyChanged', updateImage);
            C.off('stateChanged', updateImage);
            watching = false;
        }
    }

    function startWatching()
    {
        // Disconnect
        stopWatching();

        isLiveMode = false;

        // Connect
        if (settings.action == "liveMode")
        {
            watch = C.bindings.watch("global.view.liveMode", null, null, value =>
            {
                isLiveMode = value;
                updateImage();
            });
        }

        if (settings.action == "powerButton")
        {
            C.application.on('busyChanged', updateImage);
            C.on('stateChanged', updateImage);
        }

        watching = true;
    }


    return {

        onKeyUp: async function (jsonEvent)
        {
            switch (settings.action)
            {
                case "powerButton":
                    if (C.state != "connected")
                    {
                        // When starting engine, give some feedback that the button press
                        // was detected by temporarily showing the button in a busy state
                        attemptingEngineStart = true;
                        updateImage();
                        attemptingEngineStart = false;

                        // In case the engine fails to start (app not running, or options open), 
                        // reset the fake busy state after 3 seconds
                        setTimeout(updateImage, 3000);
                    }
                    else if (settings.requireLongPress)
                        break;
                    C.engine.startStop();
                    break;

                case "liveMode":
                    C.bindings.invoke("global.view.liveMode", isLiveMode ? 0 : 1);
                    break;

                case "allSoundsOff":
                    C.bindings.invoke("global.engine.allSoundsOff");
                    break;

                case "continue":
                    C.bindings.invoke("global.setList.nextSongPartInstant");
                    break;

                case "goBack":
                    $SD.api.switchToProfile(null, json.device, null);
                    break;

                case "uiCommand":
                    C.commands.invoke(settings.command);
                    break;

                case "bindingPoint":
                    // Get the binding point
                    let bps = await GetBindingPoints('target');
                    let bp = bps.find(x => x.name == settings.bindingPoint);

                    // Work out params, value etc...
                    let indicies = null;
                    let value = null;
                    let param = null;
                    if (bp.indicies?.length)
                        indicies = JSON5.parse('[' + settings.bindingPointIndicies + ']');
                    if (bp.kind != 'command' && settings.bindingPointValue != "")
                        value = JSON5.parse(settings.bindingPointValue);
                    if (bp.parameterKind != 'none' && settings.bindingPointParameter != "")
                        param = JSON5.parse(settings.bindingPointParameter);

                    // Invoke it
                    C.bindings.invoke(settings.bindingPoint, value, indicies, param);
                    break;
            }

            if (longHoldTimeout)
            {
                clearTimeout(longHoldTimeout);
                longHoldTimeout = null;
            }

        },
        onKeyDown: function (jsonEvent)
        {
            if (settings.action == "powerButton" && settings.requireLongPress && C.state == "connected")
            {
                longHoldTimeout = setTimeout(function() {
                    // Long press, toggle auto-record on/off
                    longHoldTimeout = null;
                    C.engine.stop();
                }, 1000);
            }
        },
        onDidReceiveSettings: function (jsonEvent)
        {
            settings = $SD.api.initDefaultSettings(jsonEvent, {
                action: "powerButton",
                backColor: "#000000",
                foreColor: "#848484",
                requireLongPress: false,
            });

            startWatching();
            updateImage();
        },
        onWillDisappear: function (jsonEvent)
        {
            stopWatching();
        },
        onConnectionStateChanged: function ()
        {
            updateImage();
        },
        onPropertyInspectorDidAppear: async function (jsonEvent)
        {
            let cl = await GetCommandList();
            let bp = await GetBindingPoints("target");
            $SD.api.sendToPropertyInspector(json.context, { commandList: cl, bindingPoints: bp }, json.action);
        },
    }
}