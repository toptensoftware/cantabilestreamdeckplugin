function metronomeAction(json)
{
    let settings = json.payload.settings;
    let image = new sdImage(json.context);
    let isHot = false;
    let watch = null;
    let autoRepeatInterval;

    let imageMap =
    {
        "tempoInc": "metronome_tempo_inc",
        "tempoDec": "metronome_tempo_dec",
        "tempoSet": "metronome_set_tempo",
        "tempoNextPreset": "metronome_tempo_next",
        "tempoPrevPreset": "metronome_tempo_prev",
        "timesigNextPreset": "metronome_timesig_next",
        "timesigPrevPreset": "metronome_timesig_prev",
        "timesigSet": "metronome_set_timesig",
        "soundsOnOff": "metronome_sounds"
    }

    function updateImage()
    {
        var ts = util.parseTimeSig(settings.timesig);

        image.disabled = C.state != "connected";
        image.setSvg(`images/${imageMap[settings.action]}.svg`, {
            "$(T)": settings.tempo,
            "$(n)": ts?.n ?? "?",
            "$(d)": ts?.d ?? "?",
            "#hot.display": isHot ? "show" : "none",
            "#cold.display": isHot ? "none" : "show",
            "#background.fill": settings.backColor,
            "#848484": settings.foreColor ?? "#848484",
        });
    }

    function startWatching()
    {
        stopWatching();

        isHot = false

        if (settings.action == "soundsOnOff")
        {
            watch = C.bindings.watch("global.metronome.enableSounds", null, null, value =>
            {
                isHot = value;
                updateImage();
            });
        }
    }
    
    function stopWatching()
    {
        watch?.unwatch();
        watch = null;
    }

    function doAction()
    {
        switch (settings.action)
        {
            case "tempoInc":
                C.bindings.invoke("global.metronome.increaseTempo");
                break;
                
            case "tempoDec":
                C.bindings.invoke("global.metronome.decreaseTempo");
                break;
                
            case "tempoSet":
                {
                    let t = parseInt(settings.tempo);
                    if (t < 5) t = 5;
                    if (t > 300) t = 300;
                    C.bindings.invoke("global.metronome.tempo", t);
                    break;
                }

            case "tempoNextPreset":
                C.bindings.invoke("global.metronome.nextTempoPreset");
                break;

            case "tempoPrevPreset":
                C.bindings.invoke("global.metronome.previousTempoPreset");
                break;

            case "timesigNextPreset":
                C.bindings.invoke("global.metronome.nextTimeSignaturePreset");
                break;

            case "timesigPrevPreset":
                C.bindings.invoke("global.metronome.previousTimeSignaturePreset");
                break;

            case "timesigSet":
                let ts = util.parseTimeSig(settings.timesig);
                if (ts)
                {
                    C.bindings.invoke("global.metronome.timeSignatureNumerator", ts.n);
                    C.bindings.invoke("global.metronome.timeSignatureDenominator", ts.d);
                }
                break;

            case "soundsOnOff":
                C.bindings.invoke("global.metronome.enableSounds", isHot ? 0 : 1);
                break;
        }
    }

    function shouldRepeat()
    {
        switch (settings.action)
        {
            case "tempoInc":
            case "tempoDec":
                return true;
        }
        return false;
    }


    return {

        onKeyUp: function (jsonEvent)
        {
            if (autoRepeatInterval)
            {
                clearInterval(autoRepeatInterval);
                autoRepeatInterval = null;
            }
        },
        onKeyDown: function (jsonEvent)
        {
            doAction();

            if (shouldRepeat())
            {
                autoRepeatInterval = setInterval(function ()
                {
                    doAction();
                    if (autoRepeatInterval)
                    {
                        clearInterval(autoRepeatInterval);
                        autoRepeatInterval = setInterval(doAction, 30);
                    }
                }, 500);
            }
        },
        onDidReceiveSettings: function (jsonEvent)
        {
            settings = $SD.api.initDefaultSettings(jsonEvent, {
                action: "tempoInc",
                tempo: 120,
                timesig: "4/4",
                backColor: "#000000",
                foreColor: "#848484",
            });

            startWatching();
            updateImage();
        },
        onWillDisappear: function (jsonEvent)
        {
            stopWatching();
        },
        onConnectionStateChanged: function ()
        {
            updateImage();
        }
    }
}