function stringExpressionAction(json)
{
    let settings = json.payload.settings;
    let image = new sdImage(json.context);

    let watch;

    function onText(value)
    {
        image.disabled = C.state != "connected";
        image.setText(value, settings.backColor, settings.foreColor);
    }

    function startWatching()
    {
        stopWatching();
        watch = C.variables.watch(settings.expression, onText)
    }

    function stopWatching()
    {
        watch?.unwatch();
        watch = null;
    }

    return {

        onKeyDown: function(jsonEvent)
        {
        },
        onDidReceiveSettings: function(jsonEvent)
        {
            settings = $SD.api.initDefaultSettings(jsonEvent, {
                expression: "$(SongTitle)",
                backColor: "#000000",
                foreColor: "#848484",
            });

            startWatching();
        },
        onWillDisappear: function(jsonEvent)
        {   
            stopWatching();
        },
        onConnectionStateChanged: function()
        {
            onText("-");
        }
    }
}