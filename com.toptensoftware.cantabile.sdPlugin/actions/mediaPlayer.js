function mediaPlayerAction(json)
{
    let settings = json.payload.settings;
    let image = new sdImage(json.context);
    let longHoldTimeout = false;
    let state = "stopped";
    let loopMode = "none";

    let loopModeNames = ["none", "auto", "break", "loopOnce", "loop"];
    let loopModeValues = { none: 0, auto: 1, break: 2, loopOnce: 3, loop: 4 };

    function updateImage()
    {
        let foreColor = settings.foreColor;
        let symbol;
        switch (settings.action)
        {
            case "play":
                symbol = "transport_play";
                foreColor = state == "playing" ? "#00FF00" : settings.foreColor;
                break;

            case "pause":
                symbol = "transport_pause";
                foreColor = state == "paused" ? "#FF5500" : settings.foreColor;
                break;

            case "stop":
                symbol = "transport_stop";
                break;

            case "playPause":
            case "playStop":
                switch (state)
                {
                    case "playing":
                        symbol = "transport_play";
                        foreColor = "#00FF00";
                        break;

                    case "paused":
                        symbol = "transport_pause";
                        foreColor = "#FF5500";
                        break;

                    case "stopped":
                        symbol = "transport_play";
                        break;
                }
                break;

            case "fastForwardSpeed1":
            case "fastForwardSpeed2":
                symbol = "transport_fastForward";
                break;
    
            case "rewindSpeed1":
            case "rewindSpeed2":
                symbol = "transport_rewind";
                break;

            case "loopCycleModes":
            case "loopAutoBreak":
            case "loopAutoLoopOnce":
            case "loopAutoLoop":
            case "loopBreakLoopOnce":
            case "loopBreakLoop":
                symbol = "loopmode_" + loopMode;
                break;
        
            default:
                symbol = "transport_" + settings.action;
                break;
        }

        image.disabled = C.state != "connected";
        image.setSvg(`images/${symbol}.svg`, {
            "#background.fill": settings.backColor,
            "#848484": foreColor,
        });
    }

    function onLoopModeChanged(value)
    {
        loopMode = loopModeNames[value];
        updateImage();
    }

    // Callback from Cantabile that transport state has changed
    function onStateChanged(value)
    {
        switch (value)
        {
            case 0: 
                state = "stopped";
                break;

            case 1:
                state = "playing";
                break;

            case 2:
                state = "paused";
                break;

        }
        updateImage();
    }

    let watch = null;

    function needsState()
    {
        switch (settings.action)
        {
            case "play":
            case "pause":
            case "stop":
            case "playPause":
            case "playStop":
                return true;
        }
        return false;
    }

    function needsLoopMode()
    {
        switch (settings.action)
        {
            case "loopCycleModes":
            case "loopAutoBreak":
            case "loopAutoLoopOnce":
            case "loopAutoLoop":
            case "loopBreakLoopOnce":
            case "loopBreakLoop":
                return true;
        }
        return false;
    }

    function startWatching()
    {
        stopWatching();

        state = "stopped";

        if (needsState())
        {
            watch = C.bindings.watch("global.indexedMediaPlayer.state", [0, parseInt(settings.mediaPlayerIndex)-1], null, onStateChanged);
        }
        else if (needsLoopMode())
        {
            watch = C.bindings.watch("global.indexedMediaPlayer.loopMode", [0, parseInt(settings.mediaPlayerIndex)-1], null, onLoopModeChanged);
        }
    }

    function stopWatching()
    {
        watch?.unwatch();
        watch = null;
        state = "stopped";
        loopMode = "none";
    }

    function wantsLongPress()
    {
        switch (settings.action)
        {
            case "playPause":
            case "playStop":
            case "loopBreakLoopOnce":
            case "loopBreakLoop":
                return true;
        }
        return false;
    }

    function isMomentary()
    {
        switch (settings.action)
        {
            case "fastForwardSpeed1":
            case "fastForwardSpeed2":
            case "rewindSpeed1":
            case "rewindSpeed2":
                return true;
        }
        return false;
    }

    function mapActionToBindingPoint()
    {
        // For some reason the binding point names in Cantabile for these have capital
        // leading letter.  Build 4047 and later makes the lookup of the name case-insensitive
        // but until then we need to fudge it.
        switch (settings.action)
        {
            case "fastForwardSpeed1": return "FastForwardSpeed1";
            case "fastForwardSpeed2": return "FastForwardSpeed2";
            case "rewindSpeed1": return "RewindSpeed1";
            case "rewindSpeed2": return "RewindSpeed2"
        }
        return settings.action;
    }

    function onPress()
    {
        switch (settings.action)
        {
            case "loopCycleModes":
                C.bindings.invoke(`global.indexedMediaPlayer.cycleLoopMode`, null, [0, parseInt(settings.mediaPlayerIndex)-1], null);
                break;

            case "loopAutoBreak":
                C.bindings.invoke(`global.indexedMediaPlayer.loopMode`, loopModeValues[loopMode == "auto" ? "break" : "auto"], [0, parseInt(settings.mediaPlayerIndex)-1], null);
                break;

            case "loopAutoLoopOnce":
                C.bindings.invoke(`global.indexedMediaPlayer.loopMode`, loopModeValues[loopMode == "auto" ? "loopOnce" : "auto"], [0, parseInt(settings.mediaPlayerIndex)-1], null);
                break;

            case "loopAutoLoop":
                C.bindings.invoke(`global.indexedMediaPlayer.loopMode`, loopModeValues[loopMode == "auto" ? "loop" : "auto"], [0, parseInt(settings.mediaPlayerIndex)-1], null);
                break;

            case "loopBreakLoopOnce": 
                C.bindings.invoke(`global.indexedMediaPlayer.loopMode`, loopModeValues[loopMode == "break" ? "loopOnce" : "break"], [0, parseInt(settings.mediaPlayerIndex)-1], null);
                break;

            case "loopBreakLoop": 
                C.bindings.invoke(`global.indexedMediaPlayer.loopMode`, loopModeValues[loopMode == "break" ? "loop" : "break"], [0, parseInt(settings.mediaPlayerIndex)-1], null);
                break;

            default:
                C.bindings.invoke(`global.indexedMediaPlayer.${mapActionToBindingPoint()}`, null, [0, parseInt(settings.mediaPlayerIndex)-1], null);
        }
    }

    function onLongPress()
    {
        switch (settings.action)
        {
            case "playPause":
                C.bindings.invoke(`global.indexedMediaPlayer.stop`, null, [0, parseInt(settings.mediaPlayerIndex)-1], null);
                break;

            case "playStop":
                C.bindings.invoke(`global.indexedMediaPlayer.pause`, null, [0, parseInt(settings.mediaPlayerIndex)-1], null);
                break;

            case "loopBreakLoopOnce": 
            case "loopBreakLoop": 
                C.bindings.invoke(`global.indexedMediaPlayer.loopMode`, loopModeValues["auto"], [0, parseInt(settings.mediaPlayerIndex)-1], null);
                break;
        }
    }

    return {

        onKeyUp: function (jsonEvent)
        {
            if (isMomentary())
            {
                C.bindings.invoke(`global.indexedMediaPlayer.${mapActionToBindingPoint()}`, 0, [0, parseInt(settings.mediaPlayerIndex)-1], null);
            }
            else if (!wantsLongPress() || longHoldTimeout)
            {
                clearTimeout(longHoldTimeout);
                longHoldTimeout = null;
                onPress();
            }
        },
        onKeyDown: function (jsonEvent)
        {
            if (isMomentary())
            {
                C.bindings.invoke(`global.indexedMediaPlayer.${mapActionToBindingPoint()}`, 1, [0, parseInt(settings.mediaPlayerIndex)-1], null);
            }
            else if (wantsLongPress())
            {
                longHoldTimeout = setTimeout(function() {
                    // Long press, toggle auto-record on/off
                    longHoldTimeout = null;
                    onLongPress();
                }, 1000);
            }
        },
        onDidReceiveSettings: function (jsonEvent)
        {
            settings = $SD.api.initDefaultSettings(jsonEvent, {
                action: "play",
                backColor: "#000000",
                foreColor: "#848484",
                mediaPlayerIndex: 1,
            });

            updateImage();
            startWatching();
        },
        onWillDisappear: function (jsonEvent)
        {
            C.transport.removeListener('stateChanged', onStateChanged)
            stopWatching();
        },
        onConnectionStateChanged: function ()
        {
            updateImage();
        }
    }
}