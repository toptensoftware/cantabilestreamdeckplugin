// An event emitted used to broadcast paging info
// to all active buttons.
// The emitted event will be `{deviceId}.onPageInfo`
let songPageEvents = new Cantabile.EventEmitter();
let deviceSongPageInfo = new Map();


function setlistAction(json)
{
    let settings = json.payload.settings;
    let image = new sdImage(json.context);
    let pageInfo = null;
    let watching = false;
    let isPageController = false;

    // Update the image
    function updateImage()
    {
        image.disabled = C.state != "connected";

        if (settings.action == "loadByIndex")
        {
            let resolvedIndex = resolveIndex();
            let value;
            let backColor = settings.backColor;
            let foreColor = settings.foreColor;

            // Is this a valid song index?
            if (C.setList.items && resolvedIndex >= 0 && resolvedIndex < C.setList.items.length)
            {
                // Get the song
                let s = C.setList.items[resolvedIndex];

                // Work out text to display
                if (settings.showProgramNumber)
                    value = (s.pr + 1) + "\n" + s.name;
                else
                    value = s.name;

                // Work out colors
                if (settings.showActive && C.setList?.currentSong == s)
                {
                    // The active item uses highlight colors
                    foreColor = settings.activeForeColor ?? "#FFFFFF";
                    backColor = settings.activeBackColor ?? "#0487f3";
                }
                else if (settings.useSongColor)
                {
                    // Calculate the color based on the color map from Cantabile
                    if (C.application.colors && s.color>0 && s.color < C.application.colors.length)
                    {
                        backColor = util.blendColors(backColor + "FF", C.application.colors[s.color].back);
                    }
                }

            }
            else
                value = "-";

            // Display text
            image.setText(value, backColor, foreColor);
        }
        else if (settings.action == "choose")
        {
            let display;
            let foreColor = settings.foreColor;
            let backColor = settings.backColor;

            if (C.setList?.currentSong)
            {
                let sle = C.setList.currentSong;
                if (settings.showProgramNumber)
                    display = `${sle.pr+1}\n${sle.name}`;
                else
                    display = sle.name;
                
                if (settings.useSongColor)
                {
                    if (C.application.colors && sle.color>0 && sle.color < C.application.colors.length)
                    {
                        backColor = util.blendColors(backColor + "FF", C.application.colors[sle.color].back);
                    }
                }
            }
            else if (C.song.name)
            {
                display = C.song.name;
            }
            else
            {
                display = "Choose Song";
            }

            image.setText(display, backColor, foreColor);
        }
        else
        {
            // Work out image name
            let imageName = "nav_" + settings.action;
            if (imageName.endsWith("Delayed"))
            {
                // Same image as non-delayed version
                imageName = imageName.substring(0, imageName.length - 7);
            }
            else if (settings.action == "nextPage" || settings.action == "prevPage")
            {
                // Next previous buttons can be horizontal or vertical
                let dir;
                if (settings.horizontalArrows)
                    dir = settings.action == "nextPage" ? "right" : "left";
                else
                    dir = settings.action == "nextPage" ? "down" : "up";
                imageName = `arrow_${dir}`;

            }

            // Disabled button?
            let foreColor = settings.foreColor ?? "#848484";
            if ((settings.action == "nextPage" && pageInfo && pageInfo.page >= pageInfo.pageCount-1) ||
                (settings.action == "prevPage" && pageInfo && pageInfo.page <= 0))
            {
                foreColor = util.blendColors(settings.backColor + "FF", foreColor + "40").substring(0, 7);
            }

            // Set image
            image.setSvg(`images/${imageName}.svg`, {   // `
                "#background.fill": settings.backColor,
                "#848484": foreColor,
            });
        }
    }

    // Work out the actual item index by combining our index
    // setting with the current page base
    function resolveIndex()
    {
        let ri = parseInt(settings.index);
        if (pageInfo && settings.pageable)
            ri += pageInfo.page * pageInfo.pageSize;
        return ri;
    }

    // Invoke the button action
    function doAction()
    {
        switch (settings.action)
        {
            case "choose":
                let deviceType = $SD.applicationInfo.devices.find(x=>x.id == json.device)?.type;
                switch (deviceType)
                {
                    case 0:
                        $SD.api.switchToProfile(null, json.device, "Profiles/Cantabile Choose Song");
                        break;

                    case 2:
                        $SD.api.switchToProfile(null, json.device, "Profiles/Cantabile Choose Song XL");
                        break;
                }
                break;

            case "first": 
                C.setList.loadFirstSong(false);
                break;

            case "last": 
                C.setList.loadLastSong(false);
                break;

            case "next": 
                C.setList.loadNextSong(1, false, false);
                break;

            case "prev": 
                C.setList.loadNextSong(-1, false, false);
                break;

            case "firstDelayed": 
                C.setList.loadFirstSong(true);
                break;

            case "lastDelayed": 
                C.setList.loadLastSong(true);
                break;

            case "nextDelayed": 
                C.setList.loadNextSong(1, true, false);
                break;

            case "prevDelayed": 
                C.setList.loadNextSong(-1, true, false);
                break;

            case "nextPage":
                if (pageInfo && pageInfo.page < pageInfo.pageCount - 1)
                {
                    pageInfo.page++;
                    broadcastPageInfo(pageInfo);
                }
                break;

            case "prevPage":
                if (pageInfo && pageInfo.page > 0)
                {
                    pageInfo.page--;
                    broadcastPageInfo(pageInfo);
                }
                break;

            case "loadByIndex": 
                C.setList.loadSongByIndex(resolveIndex(), false);
                if (settings.exitProfile)
                {
                    setTimeout(() => {
                        $SD.api.switchToProfile(null, json.device, null);
                    }, 300);
                }
                break;
        }
    }

    // Called when Cantabile reports something about the set list changed
    function onSetListChanged()
    {
        updateImage();
    }

    // Called when the current song changes
    function onCurrentSongChanged()
    {
        if (settings.action == "nextPage")
            updatePageInfo(true);
        updateImage();
    }

    // Called when the color table from Cantabile is loaded
    function onAppColorsChanged()
    {
        updateImage();
    }

    // Called when the current song changes
    function onSongChanged()
    {
        updateImage();
    }

    // Updates the shared page info with the number of pages
    function updatePageInfo(selectCurrentPage)
    {
        // Create default page info if none
        if (!pageInfo)
        {
            pageInfo = 
            { 
                pageCount: 0,
                pageSize: settings.pageSize,
            };
        }

        // Get total number of items
        let totalItems =  C.setList.items?.length ?? 0;

        // Calculate the total number of pages
        pageInfo.pageCount = Math.floor(totalItems / pageInfo.pageSize);
        if (totalItems % pageInfo.pageSize)
            pageInfo.pageCount++;

        // Work out new page
        if (pageInfo.page === undefined || selectCurrentPage)
            pageInfo.page = Math.floor(C.setList.currentSongIndex / pageInfo.pageSize);

        // Work out new actual offset
        if (pageInfo.page >= pageInfo.pageCount)
            pageInfo.page = pageInfo.pageCount - 1;
        if (pageInfo.page < 0)
            pageInfo.page = 0;

        // Send it
        broadcastPageInfo(pageInfo);
    }

    // Send's an updated page info to all associated buttons on the
    // same device/view
    function broadcastPageInfo(info)
    {
        deviceSongPageInfo.set(json.device, info);
        songPageEvents.emit(`${json.device}.onPageInfo`, info);
    }

    // Receives page info from another button on the same device/view
    function onPageInfo(info)
    {
        if (settings.pageable)
        {
            pageInfo = info;
            updateImage();
        }
    }

    // Start watching
    function startWatching()
    {
        // Kill old watchers
        stopWatching();

        // If this the page controller then create the page info
        // and monitor Cantabile for changes
        if (settings.action == "nextPage")
        {
            C.setList.on('changed', () => updatePageInfo(true));
            C.setList.on('currentSongChanged', onCurrentSongChanged);
            updatePageInfo();
            isPageController = true;
        }

        // These actions all participate in paging so hook to receive
        // the page info when broadcast from other buttons
        if (settings.action == 'nextPage' || 
            settings.action == 'prevPage' ||
            settings.action == 'loadByIndex' )
        {
            songPageEvents.on(`${json.device}.onPageInfo`, onPageInfo);
        }

        // If this is a explicit song button, then watch for changes
        // to the setlists
        if (settings.action == "loadByIndex")
        {
            C.setList.on('changed', onSetListChanged);
            C.setList.on('currentSongChanged', onCurrentSongChanged);
            C.application.on('changed', onAppColorsChanged);
        }

        // For choose song, we need both the set list entry or if that's null the current song object
        if (settings.action == "choose")
        {
            C.setList.on('changed', onSetListChanged);
            C.setList.on('currentSongChanged', onCurrentSongChanged);
            C.application.on('changed', onAppColorsChanged);
            C.song.on('changed', onSongChanged);
        }

        onSongChanged();

        // Open the set list
        watching = true;
    }

    function stopWatching()
    {
        // Shut down the page controller and null out the page info
        // for the current device
        if (isPageController)
        {
            broadcastPageInfo(null);
            isPageController = false;
        }

        // Remove other watchers
        if (watching)
        {
            C.setList.off('changed', onSetListChanged);
            C.setList.off('currentSongChanged', onCurrentSongChanged);
            C.application.off('changed', onAppColorsChanged);
            C.song.off('changed', onSongChanged);

            songPageEvents.off(`${json.device}.onPageInfo`, onPageInfo);
            watching = false;
        }
    }


    return {

        onKeyUp: function(jsonEvent)
        {
        },
        onKeyDown: function(jsonEvent)
        {
            doAction();
        },
        onDidReceiveSettings: function(jsonEvent)
        {
            settings = $SD.api.initDefaultSettings(jsonEvent, {
                action: "choose",
                index: 0,
                pageSize: 12,
                horizontalArrows: false,
                showProgramNumber: false,
                useSongColor: true,
                showActive: true,
                backColor: "#000000",
                foreColor: "#848484",
                activeBackColor: "#0487f3",
                activeForeColor: "#FFFFFF",
                exitProfile: false,
                pageable: true,
            });

            pageInfo = settings.pageable ? (deviceSongPageInfo.get(json.device) ?? null) : null;

            startWatching();
            updateImage();
        },
        onWillDisappear: function(jsonEvent)
        {   
            stopWatching();
        },
        onConnectionStateChanged: function()
        {
            updateImage();
        },
        onBroadcastSettings: function(jsonEvent)
        {
            // Only interested in broadcasts from this action type
            if (jsonEvent.action != "com.toptensoftware.cantabile.setlist")
                return;

            // Not interested in broadcasts from self (already received via didReceiveSettings)
            if (jsonEvent.context == json.context)
                return;

            // Quit if different device
            if (jsonEvent.device != json.device)
                return;

            // Get New Settings
            let newSettings = jsonEvent.payload.settings;

            // Must both be pageable
            if (!settings.pageable || !newSettings.pageable)
                return; 

            if (settings.action == "loadByIndex" && newSettings.action == "loadByIndex")
            {
                // Copy shared settings
                settings = Object.assign(Object.assign({}, newSettings), { index: settings.index});

                // Store settings
                $SD.api.setSettings(json.context, settings);
                updateImage();
            }

            if (settings.action.endsWith("Page") && newSettings.action.endsWith("Page"))
            {
                settings.horizontalArrows = newSettings.horizontalArrows;
                $SD.api.setSettings(json.context, settings);
                updateImage();
            }
        }
    }
}