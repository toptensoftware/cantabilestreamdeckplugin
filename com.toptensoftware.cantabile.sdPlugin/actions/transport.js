function transportAction(json)
{
    let settings = json.payload.settings;
    let image = new sdImage(json.context);
    let longHoldTimeout = false;

    function updateImage()
    {
        let foreColor = settings.foreColor;
        let symbol;
        switch (settings.action)
        {
            case "play":
                symbol = "transport_play";
                foreColor = C.transport.state == "playing" ? "#00FF00" : settings.foreColor;
                break;

            case "pause":
                symbol = "transport_pause";
                foreColor = C.transport.state == "paused" ? "#FF5500" : settings.foreColor;
                break;

            case "stop":
                symbol = "transport_stop";
                break;

            case "playPause":
            case "playStop":
                switch (C.transport.state)
                {
                    case "playing":
                        symbol = "transport_play";
                        foreColor = "#00FF00";
                        break;

                    case "paused":
                        symbol = "transport_pause";
                        foreColor = "#FF5500";
                        break;

                    case "stopped":
                        symbol = "transport_play";
                        break;
                }
                break;

            case "loopCycleModes":
            case "loopAutoBreak":
            case "loopAutoLoopOnce":
            case "loopAutoLoop":
            case "loopBreakLoopOnce":
            case "loopBreakLoop":
                symbol = "loopmode_" + C.transport.loopMode;
                break;

        }

        image.disabled = C.state != "connected";
        image.setSvg(`images/${symbol}.svg`, {
            "#background.fill": settings.backColor,
            "#848484": foreColor,
        });
    }

    // Callback from Cantabile that transport state has changed
    function onStateChanged()
    {
        updateImage();
    }

    function startWatching()
    {
        stopWatching();

        C.transport.on('stateChanged', onStateChanged);
        C.transport.on('loopStateChanged', onStateChanged);
    }

    function stopWatching()
    {
        C.transport.off('stateChanged', onStateChanged);
        C.transport.off('loopStateChanged', onStateChanged);
    }

    function wantsLongPress()
    {
        switch (settings.action)
        {
            case "playPause":
            case "playStop":
            case "loopBreakLoopOnce":
            case "loopBreakLoop":
                        return true;
        }
        return false;
    }

    function onPress()
    {
        switch (settings.action)
        {
            case "play": C.transport.play(); break;
            case "pause": C.transport.pause(); break;
            case "stop": C.transport.stop(); break;
            case "playPause": C.transport.togglePlayPause(); break;
            case "playStop": C.transport.togglePlayStop(); break;
            case "loopCycleModes": C.transport.cycleLoopMode(); break;
            case "loopAutoBreak": C.transport.loopMode = C.transport.loopMode == "auto" ? "break" : "auto"; break;
            case "loopAutoLoopOnce": C.transport.loopMode = C.transport.loopMode == "auto" ? "loopOnce" : "auto"; break;
            case "loopAutoLoop": C.transport.loopMode = C.transport.loopMode == "auto" ? "loop" : "auto"; break;
            case "loopBreakLoopOnce": C.transport.loopMode = C.transport.loopMode == "break" ? "loopOnce" : "break"; break;
            case "loopBreakLoop": C.transport.loopMode = C.transport.loopMode == "break" ? "loop" : "break"; break;
                break;
        }
    }

    function onLongPress()
    {
        switch (settings.action)
        {
            case "playPause": C.transport.stop(); break;
            case "playStop": C.transport.pause(); break;
            case "loopBreakLoopOnce": C.transport.loopMode = "auto"; break;
            case "loopBreakLoop": C.transport.loopMode = "auto"; break;
        }
    }

    return {

        onKeyUp: function (jsonEvent)
        {
            if (!wantsLongPress() || longHoldTimeout)
            {
                clearTimeout(longHoldTimeout);
                longHoldTimeout = null;
                onPress();
            }
        },
        onKeyDown: function (jsonEvent)
        {
            if (wantsLongPress())
            {
                longHoldTimeout = setTimeout(function() {
                    // Long press, toggle auto-record on/off
                    longHoldTimeout = null;
                    onLongPress();
                }, 1000);
            }
        },
        onDidReceiveSettings: function (jsonEvent)
        {
            settings = $SD.api.initDefaultSettings(jsonEvent, {
                action: "play",
                backColor: "#000000",
                foreColor: "#848484",
            });

            updateImage();
            startWatching();
        },
        onWillDisappear: function (jsonEvent)
        {
            C.transport.removeListener('stateChanged', onStateChanged)
            stopWatching();
        },
        onConnectionStateChanged: function ()
        {
            updateImage();
        }
    }
}