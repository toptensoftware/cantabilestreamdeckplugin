function countdownTimerAction(json)
{
    let settings = json.payload.settings;
    let image = new sdImage(json.context);
    let watch1 = null;
    let watch2 = null;
    let watch3 = null;
    let timerState = 0;
    let secondsTotal = 0;
    let secondsRemaining = 0;
    let flashTimer = null;
    let longHoldTimeout = false;

    // Update the button appearance
    function updateImage()
    {
        let foreColor = settings.foreColor ?? "#848484";

        switch (timerState)
        {
            case 0: // stopped
                break;

            case 1: // paused
                foreColor = "#FF5500";
                break;

            case 2: // running
                foreColor = "#00FF00";
                break;

            case 3: // finished
                if ((Date.now() % 1000) < 500)
                    foreColor = "#FF0000";
                break;
        }

        let st = secondsTotal;
        let sr = secondsRemaining;
        if (secondsTotal == 0)
        {
            st = 1;
            sr = 0;
        }
        if (sr == 0)
        {
            sr = secondsTotal;
        }

        let progressRingD = util.describeArc(19.049999 / 2, 19.049999 / 2, 8.8, 0, 360 * sr / st);

        image.disabled = C.state != "connected";
        image.setSvg(`images/countdownTimer.svg`, {
            "#progressRing.d": progressRingD,
            "#background.fill": settings.backColor,
            "#848484": foreColor,
            "$(T)": util.formatSeconds(secondsRemaining),
            "#digitalText.font-size": secondsRemaining > 3600 ? "4px" : "5px",
        });
    }

    function onTimerStateChanged(value)
    {
        timerState = parseInt(value);
        updateImage();

        if (timerState == 3)
        {
            flashTimer = setInterval(updateImage, 100);
        }
        else
        {
            clearInterval(flashTimer);
        }
    }

    // Receive total countdown period from Cantabile
    function onTimerSecondsTotal(value)
    {
        secondsTotal = parseInt(value);
        updateImage();
    }

    // Receive remaining time period from Cantabile
    function onTimerSecondsRemaining(value)
    {
        secondsRemaining = parseInt(value);
        updateImage();
    }

    // Handle the long press when stopped to switch to the next period preset
    function cycleNextPreset()
    {
        function findNextPreset()
        {
            var stops = util.parseSecondsList(settings.presets);
            for (let i = 0; i < stops.length; i++)
            {
                if (secondsTotal == stops[i])
                {
                    i++;
                    if (i >= stops.length)
                        i = 0;
                    return stops[i];
                }
            }

            if (stops.length == 0)
                return null;

            return stops[0];
        }

        let seconds = findNextPreset();
        if (seconds)
            C.bindings.invoke("global.countdownTimer.secondsTotal", seconds);
    }

    function startWatching()
    {
        stopWatching();
        watch1 = C.bindings.watch("global.countdownTimer.state", null, null, onTimerStateChanged);
        watch2 = C.bindings.watch("global.countdownTimer.secondsTotal", null, null, onTimerSecondsTotal);
        watch3 = C.bindings.watch("global.countdownTimer.secondsRemaining", null, null, onTimerSecondsRemaining);
    }

    function stopWatching()
    {
        watch1?.unwatch();
        watch1 = null;
        watch2?.unwatch();
        watch2 = null;
        watch3?.unwatch();
        watch3 = null;
    }

    return {

        onKeyUp: function (jsonEvent)
        {
            if (longHoldTimeout)
            {
                // Short press detected
                //  - when finished, reset
                //  - otherwise toggle between running and paused
                clearTimeout(longHoldTimeout);
                longHoldTimeout = null;
                if (timerState == 3)
                    C.bindings.invoke("global.countdownTimer.stop");
                else
                    C.bindings.invoke("global.countdownTimer.startPause");
            }
        },
        onKeyDown: function (jsonEvent)
        {
            longHoldTimeout = setTimeout(function ()
            {
                // Long press deteched
                //  - when running stop
                //  - when stopped select next preset
                longHoldTimeout = null;
                if (timerState == 0)
                    cycleNextPreset();
                else
                    C.bindings.invoke("global.countdownTimer.stop");
            }, 1000);
        },
        onDidReceiveSettings: function (jsonEvent)
        {
            settings = $SD.api.initDefaultSettings(jsonEvent, {
                backColor: "#000000",
                foreColor: "#848484",
                presets: "15m 20m 30m 40m 1h"
            });

            startWatching();
            updateImage();
        },
        onWillDisappear: function (jsonEvent)
        {
            stopWatching();
        },
        onConnectionStateChanged: function ()
        {
            updateImage();
        }
    }
}