function transposeAction(json)
{
    let settings = json.payload.settings;
    let image = new sdImage(json.context);
    let watch;
    let currentTranspose = null;
    let showValueUntil = 0;
    let hideValueUntil = 0;
    let valueTimer = null;
    let autoRepeatInterval;

    function updateImage()
    {
        let img = "";
        switch (settings.action)
        {
            case "transposeUp":
                img = "up";
                break;

            case "transposeDown":
                img = "down";
                break;

            case "transposeOctaveUp":
            case "transposeOctaveUpSnapped":
                img = "upOctave";
                break;

            case "transposeOctaveDown":
            case "transposeOctaveDownSnapped":
                img = "downOctave";
                break;

            case "transposeSet":
                let t = util.parseInterval(settings.transpose);
                if (t < 0)
                    img = "set_down";
                else if (t > 0)
                    img = "set_up";
                else
                    img = "set_zero";
                break;
        }

        // Load image
        image.disabled = C.state != "connected";
        image.setSvg(`images/transpose_${img}.svg`, {
            "$(T)": util.formatInterval(util.parseInterval(settings.transpose)),
            "#background.fill": settings.backColor,
            "#848484": settings.foreColor ?? "#848484",
        });
    }

    function updateTitle()
    {
        let now = Date.now();

        if (valueTimer)
            clearTimeout(valueTimer);

        // Work out whether to show value or not
        let show;
        switch (settings.showValue)
        {
            case "always":
                show = true;
                break;

            case "never":
                show = false;
                break;

            case "change":
            case "localChange":
                if (now < hideValueUntil)
                {
                    valueTimer = setTimeout(function ()
                    {
                        showValueUntil = 0;
                        updateTitle();
                    }, hideValueUntil - now);
                }
                else
                {
                    show = now < showValueUntil;
                    if (show)
                    {
                        valueTimer = setTimeout(updateTitle, showValueUntil - now);
                    }
                }
                break;
        }

        if (show)
            $SD.api.setTitle(json.context, util.formatInterval(currentTranspose));
        else
            $SD.api.setTitle(json.context, "");
    }

    // Callback from Cantabile when current transpose setting changes
    function onTransposeChanged(value)
    {
        currentTranspose = value;

        if (settings.showValue == "change")
            showValueUntil = Date.now() + 1000;

        updateTitle();
    }

    function startWatching()
    {
        watch = C.bindings.watch("global.songLevels.transpose", null, null, onTransposeChanged);
        
        // Disconnect
        stopWatching();
    }
    
    function stopWatching()
    {
        watch?.unwatch();
        watch = null;
        currentTranspose = null;
    }

    function doAction()
    {
        hideValueUntil = Date.now() + 550;
        showValueUntil = 0;

        switch (settings.action)
        {
            case "transposeUp":
            case "transposeDown":
            case "transposeOctaveUp":
            case "transposeOctaveUpSnapped":
            case "transposeOctaveDown":
            case "transposeOctaveDownSnapped":
                C.bindings.invoke("global.songLevels." + settings.action);
                $SD.broadcast(json.device, json.context, { localTransposeAdjust: true });
                break;

            case "transposeSet":
                currentTranspose = util.parseInterval(settings.transpose);
                C.bindings.invoke("global.songLevels.transpose", currentTranspose);
                $SD.broadcast(json.device, json.context, { localTransposeAdjust: true });
                break;

        }
    }

    // Chekc if this button should auto-repeat?
    function shouldRepeat()
    {
        switch (settings.action)
        {
            case "transposeUp":
            case "transposeDown":
                return true;
        }
        return false;
    }

    return {

        onKeyUp: function (jsonEvent)
        {
            if (autoRepeatInterval)
            {
                clearInterval(autoRepeatInterval);
                autoRepeatInterval = null;
            }

        },
        onKeyDown: function (jsonEvent)
        {
            doAction();

            if (shouldRepeat())
            {
                autoRepeatInterval = setInterval(function ()
                {
                    doAction();
                    if (autoRepeatInterval)
                    {
                        clearInterval(autoRepeatInterval);
                        autoRepeatInterval = setInterval(doAction, 30);
                    }
                }, 500);
            }
            updateTitle();
        },
        onDidReceiveSettings: function (jsonEvent)
        {
            settings = $SD.api.initDefaultSettings(jsonEvent, {
                action: "transposeUp",
                showValue: "localChange",
                transpose: 0,
                backColor: "#000000",
                foreColor: "#848484",
            });

            startWatching();
            updateImage();
        },
        onWillDisappear: function (jsonEvent)
        {
            stopWatching();
        },
        onConnectionStateChanged: function ()
        {
            updateImage();
        },
        onBroadcast: function (senderContext, payload)
        {
            if (payload.localTransposeAdjust)
            {
                hideValueUntil = 0;
                showValueUntil = Date.now() + 1000;
                updateTitle();
            }
        }
    }
}