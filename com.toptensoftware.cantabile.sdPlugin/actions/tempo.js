function tempoAction(json)
{
    let settings = json.payload.settings;
    let image = new sdImage(json.context);
    let watch1;
    let watch2;
    let watch3;
    let isMeasure = false;
    let isBeat = false;
    let suppressFlashUntil = 0;
    let imageOff = null;
    let imageMeasure = null;
    let imageBeat = null;

    function updateImage()
    {
        // Flash is suppressed when user is tapping button
        let suppressFlash = Date.now() < suppressFlashUntil;

        image.disabled = C.state != "connected";
        if (!suppressFlash && isMeasure && settings.flashMeasure)
        {
            image.setSvg(imageMeasure);
        }
        else if (!suppressFlash && (isBeat || isMeasure) && settings.flashBeat)
        {
            image.setSvg(imageBeat);
        }
        else
        {
            image.setSvg(imageOff);
        }
    }

    // Callback from Cantabile when in measure flash
    function onMeasure(value)
    {
        isMeasure = value;
        updateImage();
    }

    // Callback from Cantabile when in beat flash
    function onBeat(value)
    {
        isBeat = value;
        updateImage();
    }

    // Callback when title text expression changes
    function onText(value)
    {
        $SD.api.setTitle(json.context, value);
    }

    function startWatching()
    {
        stopWatching();

        if (settings.flashBeat || settings.flashTempo)
        {
            watch1 = C.bindings.watch("global.transport.flasherBeat", null, null, onBeat);
            watch2 = C.bindings.watch("global.transport.flasherMeasure", null, null, onMeasure);
        }

        $SD.api.setTitle(json.context, "");

        let expr;
        if (settings.showTimeSignature && settings.showTempo)
            expr = "$(TimeSignature) $(Tempo)";
        else if (settings.showTempo)
            expr = "$(Tempo)";
        else if (settings.showTimeSignature)
            expr = "$(TimeSignature)";

        if (expr)
            watch3 = C.variables.watch(expr, onText)
    }

    function stopWatching()
    {
        watch1?.unwatch();
        watch1 = null;
        watch2?.unwatch();
        watch2 = null;
        watch3?.unwatch();
        watch3 = null;
        isBeat = false;
        isMeasure = false;
    }

    return {

        onKeyDown: function (jsonEvent)
        {
            switch (settings.action)
            {
                case "tapTempo":
                    C.bindings.invoke("global.metronome.tapTempoDirect");
                    suppressFlashUntil = Date.now() + 1000;
                    break;

                case "nextPreset":
                    C.bindings.invoke("global.metronome.nextTempoPreset");
                    break;

                case "prevPreset":
                    C.bindings.invoke("global.metronome.previousTempoPreset");
                    break;
            }
        },
        onDidReceiveSettings: function (jsonEvent)
        {
            settings = $SD.api.initDefaultSettings(jsonEvent, {
                action: "tapTempo",
                flashBeat: true,
                flashMeasure: true,
                showTempo: true,
                showTimeSignature: true,
                backColor: "#000000",
                foreColor: "#848484",
                measureColor: "#00a500",
                beatColor: "#666666",
            });

            // Pre-calculate the image url query strings to save doing it every time
            // the beat on/off occurs
            imageOff = `images/tempo.svg?${util.encodeParams({
                "#background.fill": settings.backColor,
                "#848484": settings.foreColor ?? "#848484",
            })}`;
            imageMeasure = `images/tempo.svg?${util.encodeParams({
                "#background.fill": settings.measureColor,
                "#848484": settings.foreColor ?? "#848484",
            })}`;
            imageBeat = `images/tempo.svg?${util.encodeParams({
                "#background.fill": settings.beatColor,
                "#848484": settings.foreColor ?? "#848484",
            })}`;

            startWatching();
            updateImage();
        },
        onWillDisappear: function (jsonEvent)
        {
            stopWatching();
        },
        onConnectionStateChanged: function ()
        {
            updateImage();
        }
    }
}