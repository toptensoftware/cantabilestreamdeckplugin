function gainControlsAction(json)
{
    let settings = json.payload.settings;
    let image = new sdImage(json.context);
    let watch;
    let currentValue = null;
    let stops;
    let showValueUntil = 0;
    let hideValueUntil = 0;
    let valueTimer = null;
    let autoRepeatInterval;

    // Update button image
    function updateImage()
    {
        // If auto, select a symbol  based on target.
        let symbolName = settings.symbol;
        if (!symbolName || symbolName == "")
        {
            switch (settings.target)
            {
                case "masterOutput":
                    symbolName = "speaker";
                    break;

                case "masterInput":
                    symbolName = "microphone";
                    break;

                case "songOutput":
                    symbolName = "notes";
                    break;

                case "songInput":
                    symbolName = "microphone";
                    break;

                case "metronome":
                    symbolName = "metronome";
                    break;

                default:
                    if (settings.target && (settings.target.startsWith('in.') || settings.target.startsWith('out.')))
                        symbolName = "cable";
                    else
                        symbolName = "none";
                    break;
            }
        }

        // Load image
        image.disabled = C.state != "connected";
        image.setSvg(`images/gain_${symbolName}.svg`, {
            "#Inc.display": settings.action == "gainInc" ? "show" : "none",
            "#Dec.display": settings.action == "gainDec" ? "show" : "none",
            "#dB.display": settings.action == "gainSet" ? "show" : "none",
            "$(x)": settings.dB == "" ? "-∞" : settings.dB,
            "#background.fill": settings.backColor,
            "#848484": settings.foreColor ?? "#848484",
        });
    }

    // Update button title
    function updateTitle()
    {
        let now = Date.now();

        if (valueTimer)
            clearTimeout(valueTimer);

        // Work out whether to show value or not
        let show;
        switch (settings.showValue)
        {
            case "always":
                show = true;
                break;

            case "never":
                show = false;
                break;

            case "change":
            case "localChange":
                if (now < hideValueUntil)
                {
                    valueTimer = setTimeout(function ()
                    {
                        showValueUntil = 0;
                        updateTitle();
                    }, hideValueUntil - now);
                }
                else
                {
                    show = now < showValueUntil;
                    if (show)
                    {
                        valueTimer = setTimeout(updateTitle, showValueUntil - now);
                    }
                }
                break;
        }

        if (show)
            $SD.api.setTitle(json.context, util.formatDb(currentValue));
        else
            $SD.api.setTitle(json.context, "");
    }

    // Receive current gain setting from Cantabile
    function onValueChanged(value)
    {
        currentValue = value;

        if (settings.showValue == "change")
            showValueUntil = Date.now() + 1000;

        updateTitle();
    }

    function startWatching()
    {
        // Parse db stops
        if (settings.mode == "stops")
            stops = util.parseDbStops(settings.stops);

        watch = C.bindings.watch(bindingPointName(), null, null, onValueChanged);

        // Disconnect
        stopWatching();
    }

    function stopWatching()
    {
        watch?.unwatch();
        watch = null;
        currentValue = null;
    }

    // Get the name of the binding point associated with the user
    // selected target gain
    function bindingPointName()
    {
        switch (settings.target)
        {
            case "songOutput": return "global.songLevels.outputGain";
            case "songInput": return "global.songLevels.inputGain";
            case "masterOutput": return "global.masterLevels.outputGain";
            case "masterInput": return "global.masterLevels.inputGain";
            case "metronome": return "global.metronome.volume";
        }

        if (settings.target)
        {
            if (settings.target.startsWith('in.') || settings.target.startsWith('out.'))
            {
                return "global.masterLevels.audioPort." + settings.target;
            }
        }

        return "global.masterLevels.outputGain"
    }

    // Adjust the gain by specified amount
    function adjustGain(dir)
    {
        if (currentValue == null)
            return null;

        let oldValue = currentValue;

        if (settings.mode == "fixed")
        {
            let db;
            if (currentValue <= 0)
            {
                if (dir < 0)
                    return;
                else
                    db = -60.
            }
            else
            {
                let step = Number(settings.step) || 0.5;
                let pos = Math.round((Number(util.s2db(currentValue).toFixed(1)) / step));
                db = (pos + dir) * step;
            }
            currentValue = util.db2s(db);
            if (currentValue < 0)
                currentValue = 0;
            if (currentValue > 2.3)
                currentValue = 2.3;

            // If cross 0db boundary snap to 0db
            if ((currentValue < 1) != (oldValue < 1) && currentValue != 1 && oldValue != 1)
            {
                currentValue = 1;
            }
        }
        else
        {
            // Get next stop value
            currentValue = util.findNextStop(stops, currentValue, dir);
        }

        // Set new value (if changed)
        if (currentValue != oldValue)
        {
            C.bindings.invoke(bindingPointName(), currentValue);
            $SD.broadcast(json.device, json.context, { localGainAdjust: settings.target });
        }
    }

    // Perform the action of the button
    function doAction()
    {
        hideValueUntil = Date.now() + 550;
        showValueUntil = 0;

        switch (settings.action)
        {
            case "gainInc":
                adjustGain(1);
                break;

            case "gainDec":
                adjustGain(-1);
                break;

            case "gainSet":
                currentValue = settings.dB == "" ? 0 : util.db2s(settings.dB);
                C.bindings.invoke(bindingPointName(), currentValue);
                $SD.broadcast(json.device, json.context, { localGainAdjust: settings.target });
                break;

        }
    }

    // Check if this is an auto repeat button
    function shouldRepeat()
    {
        switch (settings.action)
        {
            case "gainInc":
            case "gainDec":
                return true;
        }
        return false;
    }

    return {

        onKeyUp: function (jsonEvent)
        {
            if (autoRepeatInterval)
            {
                clearInterval(autoRepeatInterval);
                autoRepeatInterval = null;
            }

        },
        onKeyDown: function (jsonEvent)
        {
            doAction();

            if (shouldRepeat())
            {
                autoRepeatInterval = setInterval(function ()
                {
                    doAction();
                    if (autoRepeatInterval)
                    {
                        clearInterval(autoRepeatInterval);
                        autoRepeatInterval = setInterval(doAction, 30);
                    }
                }, 500);
            }
            updateTitle();
        },
        onDidReceiveSettings: function (jsonEvent)
        {
            settings = $SD.api.initDefaultSettings(jsonEvent, {
                action: "gainInc",
                target: "masterOutput",
                symbol: "",
                showValue: "localChange",
                mode: "fixed",
                step: 0.5,
                stops: "-Inf 0 3 6 -45 -30  -20 -15 -9 -6 -3",
                dB: 0,
                backColor: "#000000",
                foreColor: "#848484",
            });

            startWatching();
            updateImage();
        },
        onWillDisappear: function (jsonEvent)
        {
            stopWatching();
        },
        onConnectionStateChanged: function ()
        {
            updateImage();
        },
        onPropertyInspectorDidAppear: async function (jsonEvent)
        {
            // When property inspector is shown, pass it a list of the
            // available audio ports
            let pl = await GetAudioPortList();
            $SD.api.sendToPropertyInspector(json.context, { portList: pl }, json.action);
        },
        onBroadcast: function (senderContext, payload)
        {
            // Show title when local adjustment from another button
            if (payload.localGainAdjust == settings.target)
            {
                hideValueUntil = 0;
                showValueUntil = Date.now() + 1000;
                updateTitle();
            }
        }

    }
}