function showNotesAction(json)
{
    let settings = json.payload.settings;
    let image = new sdImage(json.context);
    let autoRepeatInterval;

    function updateImage()
    {
        image.disabled = C.state != "connected";
        image.setSvg(`images/showNotes_${settings.action}.svg`, {
            "#background.fill": settings.backColor,
            "#848484": settings.foreColor ?? "#848484",
        });
    }

    function doScroll()
    {
        switch (settings.action)
        {
            case "lineUp":
                C.bindings.invoke("global.view.showNotesScrollUp");
                break;

            case "lineDown":
                C.bindings.invoke("global.view.showNotesScrollDown");
                break;

            case "pageUp":
                C.bindings.invoke("global.view.showNotesPageUp");
                break;

            case "pageDown":
                C.bindings.invoke("global.view.showNotesPageDown");
                break;

            case "home":
                C.bindings.invoke("global.view.showScrollPosition", 0);
                break;

            case "end":
                C.bindings.invoke("global.view.showScrollPosition", 1);
                break;
        }
    }

    function shouldRepeat()
    {
        switch (settings.action)
        {
            case "lineUp":
            case "lineDown":
            case "pageUp":
            case "pageDown":
                return true;
        }
        return false;
    }


    return {

        onKeyUp: function (jsonEvent)
        {
            if (autoRepeatInterval)
            {
                clearInterval(autoRepeatInterval);
                autoRepeatInterval = null;
            }
        },
        onKeyDown: function (jsonEvent)
        {
            doScroll();

            if (shouldRepeat())
            {
                autoRepeatInterval = setInterval(function ()
                {
                    doScroll();
                    if (autoRepeatInterval)
                    {
                        clearInterval(autoRepeatInterval);
                        autoRepeatInterval = setInterval(doScroll, 30);
                    }
                }, 500);
            }
        },
        onDidReceiveSettings: function (jsonEvent)
        {
            settings = $SD.api.initDefaultSettings(jsonEvent, {
                action: "lineUp",
                backColor: "#000000",
                foreColor: "#848484",
            });

            updateImage();
        },
        onWillDisappear: function (jsonEvent)
        {
        },
        onConnectionStateChanged: function ()
        {
            updateImage();
        }
    }
}