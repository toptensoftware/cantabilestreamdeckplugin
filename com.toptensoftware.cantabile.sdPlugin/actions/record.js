function recordAction(json)
{
    let settings = json.payload.settings;
    let image = new sdImage(json.context);
    let watch1 = C.bindings.watch("global.recorder.toggleRecording", null, null, onRecordingChanged);
    let watch2 = C.bindings.watch("global.recorder.toggleAutoRecord", null, null, onAutoRecordChanged);
    let isRecording = false;
    let isAutoRecord = false;
    let longHoldTimeout = false;

    function updateImage()
    {
        image.disabled = C.state != "connected";
        image.setSvg(`images/record.svg`, {
            "#background.fill": settings.backColor,
            "#848484": isRecording ? "#FF0000" : settings.foreColor,
            "#autoRing.display": isAutoRecord ? "show" : "none",
        });
    }

    // Received from Cantabile notification that recording state has changed
    function onRecordingChanged(value)
    {
        isRecording = value;
        updateImage();
    }

    // Received from Cantabile notification that auto-record state has changed
    function onAutoRecordChanged(value)
    {
        isAutoRecord = value;
        updateImage();
    }

    return {

        onKeyUp: function(jsonEvent)
        {
            if (longHoldTimeout)
            {
                // Short press, toggle recording on/off
                clearTimeout(longHoldTimeout);
                longHoldTimeout = null;
                C.bindings.invoke("global.recorder.toggleRecording");
            }
        },
        onKeyDown: function(jsonEvent)
        {
            longHoldTimeout = setTimeout(function() {
                // Long press, toggle auto-record on/off
                longHoldTimeout = null;
                C.bindings.invoke("global.recorder.toggleAutoRecord");
            }, 1000);
        },
        onDidReceiveSettings: function(jsonEvent)
        {
            settings = $SD.api.initDefaultSettings(jsonEvent, {
                backColor: "#000000",
                foreColor: "#848484",
            });

            updateImage();
        },
        onWillDisappear: function(jsonEvent)
        {   
            watch1.unwatch();
            watch2.unwatch();
        },
        onConnectionStateChanged: function()
        {
            updateImage();
        }
    }
}