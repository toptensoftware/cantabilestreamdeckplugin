// Simple helper to automatically binding between html controls
// and the plugin.  Works on data attributes in the html
//
// data-setting     - the name of the setting this control should be bound to
// data-show        - a javascript expression that resolves to true of false
//                    and controls the visibility of the element
// data-broadcast   - when present, whenever this setting changes it's also broadcast
//                    all other button instances via onBroadcastSettings
//
// Also, the following functions will be called if present:
//
// willLoadSettinsg - after the settings object has been received, but before
//                    field values are set.  (Use to configure field validation
//                    ranges etc)
//
// configureFields  - called whenever anything on the settings has changed

// The current settings
let settings = {};

// The connected object
let connectedObj;

// Handle connected event
$SD.on('connected', (jsonObj) => {

    // Store connected object
    connectedObj = jsonObj;

    // Ask StreamDeck for settings
    $SD.api.getSettings();

    // Find all fields with 'data-setting' attribute and connect event listeners
    // that forward the value to the plugin
    for (var f of document.querySelectorAll('[data-setting]'))
    {
        // get the setting name
        let setting = f.getAttribute('data-setting');
        let broadcast = f.getAttribute('data-broadcast') !== null;

        // Hook up listener
        switch (f.tagName)
        {
            case 'SELECT':
                f.addEventListener('change', (event) => sendValueToPlugin(event.target.value, setting, broadcast));
                break;

            case 'INPUT':
                if (f.type == 'checkbox')
                    f.addEventListener('change', (event) => sendValueToPlugin(event.target.checked, setting, broadcast));
                else
                    f.addEventListener('input', (event) => sendValueToPlugin(event.target.value, setting, broadcast));
                break;
        }
    }

    // Once everything is connected and setup, show the inspector
    const el = document.querySelector('.sdpi-wrapper');
    el.classList.remove('hidden');
});

// Callback to receive settings from the plugin. Update fields to reflect settings
$SD.on('didReceiveSettings', (jsonObj) => {

    // Store new current settings
    settings = jsonObj?.payload?.settings ?? {};

    // Call optional handler to configure fields before trying to set their values
    window.willLoadSettings?.();

    // Find all fields with 'data-setting' attribute and set the value from the
    // settings object
    for (var f of document.querySelectorAll('[data-setting]'))
    {
        // get the setting name
        let setting = f.getAttribute('data-setting');

        // If we have that setting then apply it to the field
        if (settings.hasOwnProperty(setting))
        {
            if (f.type == 'checkbox')
                f.checked = settings[setting];
            else
                f.value = settings[setting];
        }
    }

    // Update visibility of fields
    showAndHideFields();
});

// Handler for change notifications from fields forwards values to the plugin
// and hides/shows dependent fields
function sendValueToPlugin(value, param, broadcast) {

    // Store new value
    settings[param] = value;

    // Send it
    if($SD && $SD.connection) 
    {
        $SD.api.setSettings(null, settings);
        if (broadcast)
        {
            $SD.api.sendToPlugin(connectedObj.uuid, connectedObj.actionInfo.action, {
                event: "broadcastSettings",
                settings: settings,
            });
        }
    }

    // Update visibility of fields
    showAndHideFields();
}

// Show/Hide fields according to 'data-show' attribute
function showAndHideFields()
{
    // Let client script do it's stuff
    window.configureFields?.();

    // Hide/show fields
    for (var f of document.querySelectorAll('[data-show]'))
    {
        let condition = eval(f.getAttribute('data-show'));
        if (condition)
        {
            f.classList.remove('sdpi-hidden');
        }
        else
        {
            f.classList.add('sdpi-hidden');
        }
    }
}